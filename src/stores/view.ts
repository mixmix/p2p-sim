import { defineStore } from 'pinia'

interface ViewStoreState {
  showLinks: boolean

  packetSentTo: Set<number | string>
  packetArrivedAt: Set<number | string>
}

export const useViewStore = defineStore('view', {
  state: (): ViewStoreState => {
    return {
      showLinks: false,
      packetSentTo: new Set(),
      packetArrivedAt: new Set()
    }
  },
  actions: {
    toggleShowLinks () {
      this.showLinks = !this.showLinks
    },

    resetPacketView () {
      this.packetSentTo = new Set()
      this.packetArrivedAt = new Set()
    },
    addPacketSentTo (id: string | number) {
      this.packetSentTo.add(id)
    },
    addPacketArrivedTo (id: string | number) {
      this.packetArrivedAt.add(id)
    }
  }
})
