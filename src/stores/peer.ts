import { defineStore } from 'pinia'

export interface PeerStoreState {
  counter: number,
  peers: Peer[]
}

export interface Peer {
  id: PeerId
  follows: Set<number>
  isSuperConnector: boolean
}
enum PeerId {
  number,
  string
}

export interface FollowLink {
  source: PeerId,
  target: PeerId
}

export const usePeerStore = defineStore('peer', {
  state: (): PeerStoreState => ({
    counter: 0,
    peers: []
  }),
  getters: {
    count: (state) => state.peers.length,
    // TODO rename follows => followLinks
    follows: (state): FollowLink[] => {
      return state.peers.flatMap(peer => {
        return Array.from(peer.follows).map(follow => {
          return {
            source: peer.id,
            target: follow
          }
        })
      })
    },
    followers: (state) => (target: Peer): Peer[] => {
      return state.peers.filter(peer => peer.follows.has(target.id))
    },
    getFoafIds: (state) => (peer: Peer): number[] => {
      return Array.from(peer.follows)
        .flatMap(followId => state.peers.find(p => p.id === followId) || [])
        .flatMap(follow => Array.from(follow.follows))
        .filter(foafId => !peer.follows.has(foafId)) // not already following
        // NOTE this does not deduplicate at the moment
        // Fine because we're more likely to follow people others follow
    }
  },
  actions: {
    createPeer () {
      this.counter++

      const peer: Peer = {
        id: this.counter,
        follows: new Set([]),
        isSuperConnector: Math.random() <= 0.1 // 10% super-connectors
      }

      this.peers.push(peer)
      return peer
    },

    onboardPeer () {
      const [onboarder] = pickN(this.peers, 1)
      const peer = this.createPeer() // do this second so don't try picking self!
      if (!onboarder) return // first to onboard has no-one to connect with

      peer.follows.add(onboarder.id)
      onboarder.follows.add(peer.id)
    },

    // randomlyConnectPeer (peer: Peer, count: number = randomInt()) {
    //   peer.follows = new Set(
    //     pickN(this.peers, count).map(peer => peer.id)
    //   )
    // },

    followFoaf (peer: Peer) {
      this.getFoafIds(peer)
        .sort(shuffleComparator)
        .slice(0, 1)
        .forEach(foafId => peer.follows.add(foafId))
    }
  }
})

function pickN (peers: Array<Peer>, n: number): Array<Peer> {
  return peers.filter(p => (
      p.isSuperConnector || // pick from super-connctors
      Math.random() > 0.3 // and 30% of the non-super-connectors
    ))
    .sort(shuffleComparator)
    .slice(0, n) // sample
}

function shuffleComparator () {
  return Math.random() < 0.5 ? -1 : +1
}

// function randomInt (scale = 3) {
//   return Math.round(
//     scale * Math.random()
//   )
// }
