import { defineStore } from 'pinia'
import { forceSimulation, forceCenter, forceLink, forceManyBody, zoomIdentity } from 'd3'
import { SimulationNodeDatum, SimulationLinkDatum, ForceCenter, Simulation } from 'd3-force'
import { ZoomTransform } from 'd3-zoom'

export interface Node extends SimulationNodeDatum {
  id: string | number
}
export interface Link extends SimulationLinkDatum<Node> {
}

interface SimulationStoreState {
  width: number
  height: number
  simulation: Simulation<Node, Link>
  transform: ZoomTransform // NOTE not strictly a simulation - more a display preference
}

interface Coordinate {
  x: number
  y: number
}
interface Dimensions {
  width: number
  height: number
}

const CENTER_FORCE = "center"
const LINK_FORCE = "link"
const CHARGE_FORCE = "charge"

export const useSimulationStore = defineStore('simulation', {
  state: (): SimulationStoreState => {
    const center = { x: 500, y: 400 }

    const simulation: Simulation<Node, Link> = forceSimulation([])
      .velocityDecay(0.3) // default 0.4

      // force nodes towards middle of page
      .force(
        CENTER_FORCE, 
        forceCenter(center.x, center.y)
          .strength(0.05)
      )

      // force based on node links
      .force(
        LINK_FORCE,
        forceLink([])
          .id((node: Node) => node.id) // says "links reference node.id" (default: node.index)
          .distance(() => 200)
      )

      // force repulsion
      .force(
        CHARGE_FORCE, 
        forceManyBody()
          .strength(-100)
          // .distanceMin(30)
      )

    return {
      width: 1000,
      height: 800,
      simulation,
      transform: zoomIdentity,
    }
  },
  getters: {

  },
  actions: {
    setDimensions (dimensions: Dimensions) {
      this.width = dimensions.width
      this.height = dimensions.height
    },
    setCenter (center: Coordinate) {
      const force: ForceCenter<Node> | undefined = this.simulation?.force(CENTER_FORCE)
      if (!force) {
        return console.error('center force does not exist yet')
      }
      force.x(center.x)
      force.y(center.y)
    }
  }
})
