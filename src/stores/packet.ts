import { defineStore } from 'pinia'

import { Node } from './simulation.ts'
import Vector from '../lib/vector.ts'
/* eslint brace-style: ["error", "stroustrup", { "allowSingleLine": true }] */

interface PacketStoreState {
  packets: Packet[]
}

export const usePacketStore = defineStore('packet', {
  state: (): PacketStoreState => ({
    packets: []
  }),
  getters: {
  },
  actions: {
    createPacket (input: PacketInput) {
      this.packets.push(new Packet(input))
    },
    tick () {
      this.packets.forEach(packet => packet.tick())
      this.packets = this.packets
        .filter(packet => !packet.isArrived || !packet.destination)
    }
  }
})


const DISTANCE_PER_TICK = 5
const ARRIVAL_RADIUS = 10

/* NOTES
  - looks weird when peers move... I think because I haven't integrated
    velocity / acceleration, so movement doesn't feel "physical"
  - if the "from Peer" moves towards "to Peer" faster than DISTANCE_PER_TICK,
    then packets will arrive out of sequence, and replication will (this time)
*/

interface PacketInput {
  from: Node
  to: Node
  onArrival?: () => void
  hopsLeft: number
  seen?: Set<number>
}

function noop () {}

export default class Packet {
  location: Vector
  destination: Node
  isArrived: boolean
  onArrival: () => void
  hopsLeft: number
  // the places this packet has been
  seen?: Set<number | string>

  constructor ({ from, to, onArrival, hopsLeft, seen }: PacketInput)  {
    this.destination = to
    if (!from.x || !from.y) throw new Error('no coords')

    this.location = new Vector(from.x, from.y)
    this.isArrived = false
    this.onArrival = onArrival || noop
    this.hopsLeft = hopsLeft || 2
    this.seen = seen || new Set()

    this.seen.add(from.id)
  }

  tick () {
    if (!this.destination.x || !this.destination.y) return
    // calculate vector: packet --> to
    const vector = new Vector(this.destination.x, this.destination.y)
      .subtract(this.location)

    const d = vector.length
    if (d <= ARRIVAL_RADIUS) {
      this.isArrived = true
      this.onArrival()
      return
    }

    const vectorStep = vector.scale(DISTANCE_PER_TICK / d)
      // could use vector.scaleTo, but this saves calculating vector.length again

    this.location.add(vectorStep)
  }

  // isEqual (packet) {
  //   return (
  //     this.from.id === packet.from.id &&
  //     this.to.id === packet.to.id &&
  //     this.msg.author === packet.msg.author &&
  //     this.msg.seq === packet.msg.seq
  //   )
  // }
}
