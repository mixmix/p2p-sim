export default class Vector {
  x: number
  y: number

  constructor (x: number, y: number) {
    this.x = x
    this.y = y
  }

  get length () {
    return Math.sqrt(Math.pow(this.x, 2) + Math.pow(this.y, 2))
  }

  // get unit () {
  //   const len = this.length
  //   return [this.x / len, this.y / len]
  // }

  /* mutating methods */
  add (vector: Vector) {
    this.x = this.x + vector.x
    this.y = this.y + vector.y
    return this
  }

  subtract (vector: Vector) {
    this.x = this.x - vector.x
    this.y = this.y - vector.y
    return this
  }

  scale (k: number) {
    this.x = k * this.x
    this.y = k * this.y
    return this
  }

  scaleTo (k: number) {
    // shrink is to a unit vector, then scale that up to length k
    // this.scale(1 / this.length).scale(k)
    return this.scale(k / this.length)
  }

  copy () {
    return new Vector(this.x, this.y)
  }
}
