# p2p-sim

This project is designed to help people explore how data moves in a p2p system.

It's modelled on Scuttlebutt's data replication patterns where peers "follow"
other peers (signalling their intention to replicate publically), and then
messages are replicated out to "2 hops" from their source (to people who follow
the source, and people who follow them)

Simulating growth of the network follows the following algorithm:
- "onboard" 100 peers
  - attach a peer to the graph by creating a mutual follow between it an
    existing peer
  - mark some (10%) of peers as "super-connectors" and bias towards these being
    the onboarders
- each second, have each peer has a chance of following another peer in it's
  neighbourhood
  - choose a peer that is a "friend of a friend" (someone who someone you follow
    follows). The effect is bringing a 2-hop follow to a 1-hop follow
  - have the chance of new friends being added be inversely proportional to the
    number of people you already follow (creates a Dunbar's number effect)

```bash
npm install
npm run dev
```

Limitations of the model:
- not all peers are online + connected to one another all the time
  - TODO add a layer of "network connection" on top of "follow graph"
- would like to use:
  - real social graph from scuttlebutt
  - real scuttlebutt nodes
    - watch EBT vs classic replication!


---

_template notes_

# Vue 3 + TypeScript + Vite

This template should help get you started developing with Vue 3 and TypeScript in Vite. The template uses Vue 3 `<script setup>` SFCs, check out the [script setup docs](https://v3.vuejs.org/api/sfc-script-setup.html#sfc-script-setup) to learn more.

## Recommended IDE Setup

- [VS Code](https://code.visualstudio.com/) + [Volar](https://marketplace.visualstudio.com/items?itemName=Vue.volar) (and disable Vetur) + [TypeScript Vue Plugin (Volar)](https://marketplace.visualstudio.com/items?itemName=Vue.vscode-typescript-vue-plugin).

## Type Support For `.vue` Imports in TS

TypeScript cannot handle type information for `.vue` imports by default, so we replace the `tsc` CLI with `vue-tsc` for type checking. In editors, we need [TypeScript Vue Plugin (Volar)](https://marketplace.visualstudio.com/items?itemName=Vue.vscode-typescript-vue-plugin) to make the TypeScript language service aware of `.vue` types.

If the standalone TypeScript plugin doesn't feel fast enough to you, Volar has also implemented a [Take Over Mode](https://github.com/johnsoncodehk/volar/discussions/471#discussioncomment-1361669) that is more performant. You can enable it by the following steps:

1. Disable the built-in TypeScript Extension
   1. Run `Extensions: Show Built-in Extensions` from VSCode's command palette
   2. Find `TypeScript and JavaScript Language Features`, right click and select `Disable (Workspace)`
2. Reload the VSCode window by running `Developer: Reload Window` from the command palette.
